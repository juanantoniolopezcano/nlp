# NLP

Práctica del módulo de NLP del BootCamp Full Stack Big Dat AI y ML VI

Enlace datasets: 

## Electrónica
http://snap.stanford.edu/data/amazon/productGraph/categoryFiles/reviews_Electronics_5.json.gz

## Bebes
http://snap.stanford.edu/data/amazon/productGraph/categoryFiles/reviews_Baby_5.json.gz

## Deportes y aire libre
http://snap.stanford.edu/data/amazon/productGraph/categoryFiles/reviews_Sports_and_Outdoors_5.json.gz

## Salud y cuidado personal
http://snap.stanford.edu/data/amazon/productGraph/categoryFiles/reviews_Health_and_Personal_Care_5.json.gz

# TOPICS MODELLING

El modelo (TopicModellin1_JuanAntonioLopezCano.ipynb) lo realizo con un subset del dataset de 200000 instancias ya que con el total del dataset  google colab se reiniciaba porque habi consumido toda la memoria ram. Con un diccionario de 76285 palabras, despues del repprocesado, entreno un modelo de cuatro topics, uno por cada dataset, tiene logica, pero al analizar el resultado de modelo, muestro el top 20 de cada topics y compruebo que si obttengo un patron que relaciona los topics con los datasets descargados, a lo mejor el saber de que van los dataset desde el principio hace que vea ese patron, si no supieran de que son lo mismo observaria otros patrones. Realizo un cluster de cual sería mi numero optimo de topics segun mis datos, la gráfica me muestra que cinco seria mi número de topic idoneo.
Entreno otro modelo con el numero de topics igual a cinco (TopicModellin1.1_JuanAntonioLopezCano.ipynb) el cual muestra segun mi opinion mejores resultados, el número optimo son 5 segun el cluster, observo que el quinto topic es una mezcla de dos o tres datasets.

Realizo otro modelo donde le indico ocho numero de topic inicialmente (TopicModellin2_JuanAntonioLopezCano.ipynb), se me ocurrio entrenar un modelo con el doble de topics de los dataset inicles a ver si encontraba otros patrones, el resultado del modelo no me dice nada observo una mezcla de los cuatro datasets, realizo el cluester y me diece que son siete el número de clusters optimos.
De nuevo entreno otro modelo (TopicModellin2.2_JuanAntonioLopezCano.ipynb), con el nbumero de topics optimos obtenido en el anterior modelo y ya si que al ver el html obtenido con pyLDAvis esto es un desastre, no entiendo nada, jajajajaj.

Conclusión: 
Los modelos no son buenos pero si tuviera que quedarme con algunp, seria con el (TopicModellin1_JuanAntonioLopezCano.ipynb) ya que es el que mejor resultado ofrece, en mi opinion por lo que comntaba anterirmente, que ya se desde el inicio los datos que contiene el dataset, ademoas creo que son datos los procesados para el modelo de prediccion, volvere a hacer este ejercicio con otrso datos y valorare los resultados.

# SENTIMENTAL ANALISIS

Lo mismo que en el ejercicio anterior, realizo el modelo de predicción con 200000 instancias por el problema de colab con la ram.
Realizo dos modelos de analisis de sentimiento, en el primero (SentimentAnalisis_1.1_JuanAntonioLopezCano.ipynb) en el preprocesado decido que 1,2,3 sean reviews negativas y 4,5 positivos, obtengo un acuracy de [Accuracy for C=10000: 0.82646]
En el segundo (SentimentAnalisis_1.2_JuanAntonioLopezCano.ipynb) en el preprocesado decido que 1,2 sean reviews negativas y 3,4,5 positivos, obtengo un acuracy de [Accuracy for C=10000: 0.90328]

Conclusión:
Observo que el modelo 1.1 y el modelo 1.2 son muy parecidos, pero el modelo 1.2 indica que valida los negativos y los positivos de la mismoa forma, creo esta bastante balanceado, la grafica del modelo 1.1 se observa algo parecido pero con menos proyección. Todo paece indicar que con mas datos este modelo sería mas fiable bajo mi punto de vista, amazon se encarga de que sus reviews sean estructuradas y tengan los menos errores posible. Es lo que interpreto, de todas formas me gustaría profundizar mas en este asunto y hacer mas pruebas, a ver como comprobaras he utilizado tu codigo pero no tenia tiempo de hacerlo yo, ademas siempre nos dicen que algo que esta hecho y funciona, para que hacerlo de nuevo, tambien debería a¡haber preprocesado mas pero los datos de amazon los veia bastante limpios, solo con preprocesar los puntos, simn¡bolos e idioma creia bastante suficiente.


#### Opinion personal del módulo.
Te diría que es el módulo que mas me ha gustado y en el que voy a especializarme, tengo que estudiar mas y meterme mas en profundidad, porque no se si lo que he hecho en esta practica estaria bien o he dicho cosas que son una burrada, pero asi es como lo veo.
Gracias por todo y estamos en contacto.







